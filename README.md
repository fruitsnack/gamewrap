# gamewrap

A game wrapper with common linux gaming and optimization tools.

## Requirements

- gamescope
- gamemode
- mangohud
- vkbasalt
- antimicrox
- xrandr

## Usage

Consult script's --help output for usage.  

## License
This script is licensed under coffeeware (fork of beerware) license.
